from unittest import TestCase
from myarray.basearray import *

class test_init_(TestCase):
    def test_izpis(self):
        a = BaseArray((3, 3), dtype=int, data=(-3, -2, -1, 0, 1, 2, 3, 4, 5))
        b = BaseArray((3, 4), dtype=int, data=(3, 8, 1, 1, -4, -2, -3, -4, -5,3, 12, -10))
        c = BaseArray((5, 2), dtype=float, data=(-5.0, 5.00, 5.001, 5, 5, 5.0, 5.00, 5.001, 5, 5))
        d = BaseArray((3, 3, 3), dtype=int, data=(1,2,333,4,-5,1,7,8,1,1,2,3,4,5,1,7,8,1,1,2,3,4,5,1,7,8,1111))
        self.assertEqual(a.izpis(), "-3 -2 -1 \n 0  1  2 \n 3  4  5 ")
        self.assertEqual(b.izpis(), " 3   8   1   1  \n-4  -2  -3  -4  \n-5   3   12 -10 ")
        self.assertEqual(c.izpis(),"-5.0   5.0  \n 5.001 5    \n 5     5.0  \n 5.0   5.001\n 5     5    ")
        self.assertEqual(d.izpis(), " 1    4    7   \n 1    4    7   \n 1    4    7   \n\n 2   -5    8   \n 2    5    8"
                                    "   \n 2    5    8   \n\n 333  1    1   \n 3    1    1   \n 3    1    1111\n")

    def test_sortiranje(self):
        f = BaseArray((3, 3), dtype=int, data=(0, 1, 1, 4, 0, -2, 1, 3, 0))
        f.sortiranje("vrstica")
        self.assertEqual(f.data(), [0, 1, 1, -2, 0, 4, 0, 1, 3])
        g = BaseArray((3, 3), dtype=int, data=(0, 1, 1, 4, 0, -2, 1, 3, 0))
        g.sortiranje("stolpec")
        self.assertEqual(g.data(), [0, 0, -2, 1, 1, 0, 4, 3, 1])
        c = BaseArray((5,), dtype=float, data=(-5.0, 5.00, 5.001, 5, 5))
        c.sortiranje("vrstica")
        self.assertEqual(c.data(), [-5.0, 5.00, 5.001, 5, 5])
        d = BaseArray((5,), dtype=float, data=(-5.0, 5.00, 5.001, 5, 5))
        d.sortiranje("stolpec")
        self.assertEqual(d.data(), [-5.0, 5.00, 5.001, 5, 5])

        with self.assertRaises(Exception) as cm:
            d.sortiranje("stolpeca")
        self.assertEqual(('stolpeca je napacen tip. Sprejemljiva tipa sta:\n - vrstica\n - stolpec',),
                         cm.exception.args)

    def test_iskanje(self):
        c = BaseArray((5,), dtype=float, data=(-5.0, 5.00, 5.001, 5, 5))
        self.assertEqual(c.iskanje(5.001), "((2))")
        self.assertEqual(c.iskanje(5), "((1), (3), (4))")

        a = BaseArray((3, 3), dtype=int, data=(-3, -2, -1, 0, 1, 2, 3, 4, 5))
        self.assertEqual(a.iskanje(2), "((1, 2))")

        d = BaseArray((3, 3, 3), dtype=int,
                      data=(1, 2, 333, 4, -5, 1, 7, 8, 1, 1, 2, 3, 4, 5, 1, 7, 8, 1, 1, 2, 3, 4, 5, 1, 7, 8, 1111))
        self.assertEqual(d.iskanje(1), "((0, 0, 0), (0, 1, 2), (0, 2, 2), (1, 0, 0), (1, 1, 2), (1, 2, 2), (2, 0, 0), (2, 1, 2))")

        b = BaseArray((3, 4), dtype=int, data=(3, 8, 1, 1, -4, -2, -3, -4, -5, 3, 12, -10))
        self.assertEqual(b.iskanje(2), "Stevilo ne obstaja")

    def test_sestej(self):
        a = 1
        b = 2
        self.assertEqual(sestej(a,b, 1, 1), 3)

        a = BaseArray((3, 3), dtype=int, data=(-3, -2, -1, 0, 1, 2, 3, 4, 5))
        b = 2
        rezultat = sestej(a.data(),b,a.shape,1)
        self.assertEqual(rezultat.data(), [-1, 0, 1, 2, 3, 4, 5, 6, 7])

        a = BaseArray((3, 3), dtype=int, data=(-3, -2, -1, 0, 1, 2, 3, 4, 5))
        b = BaseArray((3, 3), dtype=int, data=(-3, -2, -1, 0, 1, 2, 3, 4, 5))
        rezultat = sestej(a.data(), b.data(), a.shape, b.shape)
        self.assertEqual(rezultat.data(), [-6, -4, -2, 0, 2, 4, 6, 8, 10])

    def test_odstej(self):
        a = 1
        b = 2
        self.assertEqual(odstej(a, b, 1, 1), -1)

        a = BaseArray((3, 3), dtype=int, data=(-3, -2, -1, 0, 1, 2, 3, 4, 5))
        b = 2
        rezultat = odstej(a.data(), b, a.shape, 1)
        self.assertEqual(rezultat.data(), [-5, -4, -3, -2, -1, 0, 1, 2, 3])

        a = BaseArray((3, 3), dtype=int, data=(-3, -2, -1, 0, 1, 2, 3, 4, 5))
        b = BaseArray((3, 3), dtype=int, data=(-3, -2, -1, 0, 1, 2, 3, 4, 5))
        rezultat = odstej(a.data(), b.data(), a.shape, b.shape)
        self.assertEqual(rezultat.data(), [0, 0, 0, 0, 0, 0, 0, 0, 0])

    def test_deli(self):
        a = 1
        b = 2
        self.assertEqual(deli(a, b, 1, 1), 0.5)

        a = BaseArray((3, 3), dtype=int, data=(-3, -2, -1, 0, 1, 2, 3, 4, 5))
        b = 2
        rezultat = deli(a.data(), b, a.shape, 1)
        self.assertEqual(rezultat.data(), [-1.5, -1, -0.5, 0, 0.5, 1, 1.5, 2, 2.5])

        a = BaseArray((3, 3), dtype=int, data=(-3, -2, -1, 5, 1, 2, 3, 4, 5))
        b = BaseArray((3, 3), dtype=int, data=(-3, -2, -1, 5, 1, 2, 3, 4, 5))
        rezultat = deli(a.data(), b.data(), a.shape, b.shape)
        self.assertEqual(rezultat.data(), [1, 1, 1, 1, 1, 1, 1, 1, 1])

        with self.assertRaises(Exception) as cm:
            a = BaseArray((3, 3), dtype=int, data=(-3, -2, -1, 0, 1, 2, 3, 4, 5))
            b = BaseArray((3, 3), dtype=int, data=(-3, -2, -1, 0, 1, 2, 3, 4, 5))
            deli(a.data(), b.data(), a.shape, b.shape)
        self.assertEqual(('Deljenje z 0 ni dovoljeno',), cm.exception.args)

        with self.assertRaises(Exception) as cm:
            a = 1
            b = 0
            deli(a, b, 1, 1)
        self.assertEqual(('Deljenje z 0 ni dovoljeno',), cm.exception.args)

    def test_logaritmiraj(self):
        a = 2
        b = 10
        self.assertEqual(logaritmiraj(a, b, 1, 1), 0.30102999566398114)

        a = BaseArray((3, 3), dtype=int, data=(3, 2, 0.1, 1, 1, 2, 3, 4, 5))
        b = 10
        rezultat = logaritmiraj(a.data(), b, a.shape, 1)
        self.assertEqual(rezultat.data(), [0.47712125471966244, 0.30102999566398114, -0.9999999999999998, 0.0, 0.0,
                                           0.30102999566398114, 0.47712125471966244, 0.6020599913279623,
                                           0.6989700043360187])

        with self.assertRaises(Exception) as cm:
            a = BaseArray((3, 3), dtype=int, data=(3, 2, 0, 1, 1, 2, 3, 4, 5))
            b = 10
            logaritmiraj(a.data(), b, a.shape, 1)
        self.assertEqual(('Logaritmant ne sme biti manjsi ali enak 0',), cm.exception.args)

        with self.assertRaises(Exception) as cm:
            a = BaseArray((3, 3), dtype=int, data=(3, 2, 0, 1, 1, 2, 3, 4, 5))
            b = 0
            logaritmiraj(a.data(), b, a.shape, 1)
        self.assertEqual(('Osnova ne sme biti manjsa ali enaka 0',), cm.exception.args)

    def test_potenciraj(self):
        a = 2
        b = 2
        self.assertEqual(potenciraj(a, b, 1, 1), 4)

        a = BaseArray((3, 3), dtype=int, data=(-3, -2, -1, 0, 1, 2, 3, 4, 5))
        b = 2
        rezultat = potenciraj(a.data(), b, a.shape, 1)
        self.assertEqual(rezultat.data(), [9, 4, 1, 0, 1, 4, 9, 16, 25])

    def test_mnozi(self):
        a = BaseArray((3, 3), dtype=int, data=(-3, -2, -1, 5, 1, 2, 3, 4, 5))
        b = BaseArray((3, 3), dtype=int, data=(-3, -2, -1, 5, 1, 2, 3, 4, 5))
        c = a.mnozi(b, b.shape)
        self.assertEqual(c.data(), [-15, -6, -6, 25, 3, 12, 15, 12, 30])

        with self.assertRaises(Exception) as cm:
            a = BaseArray((3, 3), dtype=int, data=(3, 2, 0, 1, 1, 2, 3, 4, 5))
            b = BaseArray((4, 3), dtype=int, data=(-3, -2, -1, 5, 1, 2, 3, 4, 5, 4, 4, 4))
            c = a.mnozi(b, b.shape)
        self.assertEqual(('Nepravilna velikost matrik (3, 3), (4, 3)',), cm.exception.args)

        with self.assertRaises(Exception) as cm:
            a = BaseArray((3, 4), dtype=int, data=(3, 2, 0, 1, 1, 2, 3, 4, 5, 4, 4, 4))
            b = BaseArray((3, 3), dtype=int, data=(-3, -2, -1, 5, 1, 2, 3, 4, 4))
            c = a.mnozi(b, b.shape)
        self.assertEqual(('Nepravilna velikost matrik (3, 4), (3, 3)',), cm.exception.args)
