from typing import Tuple
from typing import List
from typing import Union
import math


class BaseArray:
    """
    Razred BaseArray, za učinkovito hranjenje in dostopanje do večdimenzionalnih podatkov. Vsi podatki so istega
    številskega tipa: int ali float.

    Pri ustvarjanju podamo obliko-dimenzije tabele. Podamo lahko tudi tip hranjenih podatkov (privzeto je int) in
    podatke za inicializacijo (privzeto vse 0).

    Primeri ustvarjanja:
    a = BaseArray((10,)) # tabela 10 vrednosti, tipa int, vse 0
    a = BaseArray((3, 3), dtype=float) # tabela 9 vrednost v 3x3 matriki, tipa float, vse 0.0
    a = BaseArray((2, 3), dtype=float, dtype=float, data=(1., 2., 1., 2., 1., 2.)) # tabela 6 vrednost v 2x3 matriki,
                                                                               #tipa float, vrednosti 1. in 2.
    a = BaseArray((2, 3, 4, 5), data=tuple(range(120))) # tabela dimenzij 2x3x4x5, tipa int, vrednosti od 0 do 120


    Do podatkov dostopamo z operatorjem []. Pri tem moramo paziti, da uporabimo indekse, ki so znotraj oblike tabele.
    Začnemo z indeksom 0, zadnji indeks pa je dolžina dimenzije -1.
     Primeri dostopanja :

    a = BaseArray((2, 3, 4, 5))
    a[0, 1, 2, 1] = 10 # nastavimo vrednost 10 na mesto 0, 1, 2, 1
    v = a[0, 0, 0, 0] # preberemo vrednost na mestu 0, 0, 0 ,0

    a[0, 3, 0, 0] # neveljaven indeks, ker je vrednost 3 enaka ali večja od dolžine dimenzije

    Velikost tabele lahko preberemo z lastnostjo 'shape', podatkovni tip pa z 'dtype'. Primer:

    a.shape # to vrne vrednost  (2, 3, 4, 5)
    a.dtype # vrne tip int

    Čez tabelo lahko iteriramo s for zanko, lahko uporabimo operatorje/ukaze 'reversed' in 'in'.

    a = BaseArray((4,2), data=(1, 2, 3, 4, 5, 6, 7, 8))
    for v in a: # for zanka izpiše vrednost od 1 do 8
        print(v)
    for v in reversed(a): # for zanka izpiše vrednost od 8 do 1
        print(v)

    2 in a # vrne True
    -1 in a # vrne False
    """
    def __init__(self, shape: Tuple[int], dtype: type=None, data: Union[Tuple, List]=None):
        """
        Create an initialize a multidimensional myarray of a selected data type.
        Init the myarray to 0.

        :param shape: tuple or list of integers, shape of constructed myarray
        :param dtype: type of data stored in myarray, float or int
        :param data: list of tuple of data values, must contain consistent types and
                     have a length that matches the shape.
        """

        if not isinstance(shape, (tuple, list)):
            raise Exception(f'shape {shape:} is not a tuple or list')
        for v in shape:
            if not isinstance(v, int):
                raise Exception(f'shape {shape:} contains a non integer {v:}')

        self.__shape = (*shape,)

        self.__steps = _shape_to_steps(shape)

        if dtype is None:
            dtype = float
        if dtype not in [int, float]:
            raise Exception('Type must by int or float.')
        self.__dtype = dtype

        n = 1
        for s in self.__shape:
            n *= s

        if data is None:
            if dtype == int:
                self.__data = [0]*n
            elif dtype == float:
                self.__data = [0.0]*n
        else:
            if len(data) != n:
                raise Exception(f'shape {shape:} does not match provided data length {len(data):}')

            for d in data:
                if not isinstance(d, (int, float)):
                    raise Exception(f'provided data does not have a consistent type')
            self.__data = [d for d in data]

    @property
    def shape(self):
        """
        :return: a tuple representing the shape of the myarray.
        """
        return self.__shape

    @property
    def dtype(self):
        """
        :return: the type stored in the myarray.
        """
        return self.__dtype

    def __test_indice_in_range(self, ind):
        """
        Test if the given indice is in within the range of this myarray.
        :param ind: the indices used
        :return: True if indices are valid, False otherwise.
        """
        for ax in range(len(self.__shape)):
            if ind[ax] < 0 or ind[ax] >= self.shape[ax]:
                raise Exception('indice {:}, axis {:d} out of bounds (0, {:})'.format(ind, ax, self.__shape[ax]))

    def __getitem__(self, indice):
        """
        Return a value from this myarray.
        :param indice: indice to this myarray, integer or tuple
        :return: value at indice
        """
        if not type(indice) is tuple:
            indice = (indice,)

        if not _is_valid_indice(indice):
            raise Exception(f'{indice} is not a valid indice')

        if len(indice) != len(self.__shape):
            raise Exception(f'indice {indice} is not valid for this myarray')

        self.__test_indice_in_range(indice)
        key_lin = _multi_to_lin_ind(indice, self.__steps)
        return self.__data[key_lin]

    def __setitem__(self, indice, value):
        """
        Set a value in this myarray.
        :param indice: valued indice to a position, integer of tuple
        :param value: value to set
        :return: does not return
        """
        if not type(indice) is tuple:
            indice = (indice,)

        if not _is_valid_indice(indice):
            raise Exception(f'{indice} is not a valid indice')

        if len(indice) != len(self.__shape):
            raise Exception(f'indice {indice} is not valid for this myarray')

        self.__test_indice_in_range(indice)
        key_lin = _multi_to_lin_ind(indice, self.__steps)
        self.__data[key_lin] = value

    def __iter__(self):
        for v in self.__data:
            yield v

    def __reversed__(self):
        for v in reversed(self.__data):
            yield v

    def __contains__(self, item):
        return item in self.__data

    def izpis(self):
        dimenzije = self.shape
        z = dimenzije[0]
        y = dimenzije[1]
        if len(dimenzije) > 2:
            x = dimenzije[2]
        else:
            x = 1
        tip = 0 # 0=INT 1=FLOAT
        if self.dtype == float:
            tip = 1
        najdaljsi = 0
        obstaja_negativen = 0
        for m in self:
                if len(str(m)) > najdaljsi:
                    najdaljsi = len(str(m))
                if m < 0:
                    obstaja_negativen = 1
        niz = ""
        if len(dimenzije) == 2:
            for i in range(0, z):
                for j in range(0, y):
                    if obstaja_negativen == 1 and self[i, j] >= 0:
                        niz += " "
                    niz += str(self[i, j])
                    if obstaja_negativen == 0:
                        niz += " "
                    if self[i, j] >= 0:
                        niz += " " * (najdaljsi - len(str(self[i, j])))
                    if self[i, j] < 0:
                        niz += " " * (najdaljsi - len(str(self[i, j])) + 1)
                niz += "\n"
            niz = niz[:-1]
            return niz
        for k in range(0, x):
            for i in range(0, z):
                for j in range(0, y):
                    if obstaja_negativen == 1 and self[i, j, k] >= 0:
                        niz += " "
                    niz += str(self[i, j, k])
                    if obstaja_negativen == 0:
                        niz += " "

                    if self[i, j, k] >= 0:
                        niz += " " * (najdaljsi - len(str(self[i, j, k])))
                    if self[i, j, k] < 0:
                        niz += " " * (najdaljsi - len(str(self[i, j, k])) + 1)
                niz += "\n"
            niz += "\n"
        niz = niz[:-1]
        return niz

    def sortiranje(self, tip):
        sklad = []
        if len(self.shape) != 1:
            if tip == "vrstica":

                skladTMP2 = []
                for i in range(0, self.shape[0]):
                    skladTMP = []
                    for j in range(0, self.shape[1]):
                        skladTMP.append(self[i, j])
                    skladTMP2.append(skladTMP)
                urejen = []
                for i in skladTMP2:
                    urejen.append(merge_sort(i))
                sklad = urejen
                sklad_za_vracanje = []
                for i in sklad:
                    for j in i:
                        sklad_za_vracanje.append(j)
                self.__data = sklad_za_vracanje
            elif tip == "stolpec":
                skladTMP2 = []
                for j in range(0, self.shape[1]):
                    skladTMP = []
                    for i in range(0, self.shape[0]):
                        skladTMP.append(self[i, j])
                    skladTMP2.append(skladTMP)
                urejen = []
                for i in skladTMP2:
                    urejen.append(merge_sort(i))
                sklad = urejen
                sklad_za_vracanje = []
                velikost_podsklada = len(sklad[0])
                for j in range(0, velikost_podsklada):
                    for i in range(0, len(sklad)):
                        tmp = sklad[i]
                        sklad_za_vracanje.append(tmp[j])
                self.__data = sklad_za_vracanje
            else:
                raise Exception('{:s} je napacen tip. Sprejemljiva tipa sta:\n - vrstica\n - stolpec'.format(tip))
        else:
            if tip == "vrstica":
                self._data = merge_sort(sklad)
            elif tip == "stolpec":
                pass
            else:
                raise Exception('{:s} je napacen tip. Sprejemljiva tipa sta:\n - vrstica\n - stolpec'.format(tip))

    def iskanje(self, stevilo):
        obstaja = 0
        izhod = "("
        if len(self.shape) == 1:
            for i in range(0, self.shape[0]):
                if self[i] == stevilo:
                    if obstaja == 1:
                        izhod += ", "
                    obstaja = 1
                    izhod += "(" + str(i) + ")"
        elif len(self.shape) == 2:
            for j in range(0, self.shape[0]):
                for i in range(0, self.shape[1]):
                    if self[j, i] == stevilo:
                        if obstaja == 1:
                            izhod += ", "
                        obstaja = 1
                        izhod += ("(" + str(j) + ", " + str(i) + ")")

        elif len(self.shape) == 3:
            for i in range(0, self.shape[0]):
                for j in range(0, self.shape[1]):
                    for k in range(0, self.shape[2]):
                        if self[i, j, k] == stevilo:
                            if obstaja == 1:
                                izhod += ", "
                            obstaja = 1
                            izhod += "(" + str(i) + ", " + str(j) + ", " + str(k) + ")"
        else:
            print("PREVELIKA TABELA")
            return -1
        if obstaja == 1:
            izhod += ")"
        else:
            izhod = "Stevilo ne obstaja"
        return izhod

    def data(self):
        izhod = []
        for a in self:
            izhod.append(a)
        return izhod

    def mnozi(self, matrika2, velikost2):
        izhod = []
        velikost1 = self.shape
        if velikost1[1] != velikost2[0]:
            raise Exception("Nepravilna velikost matrik {:}, {:}".format(velikost1, velikost2))
        for x in range(0, self.shape[0]):
            for y in range(velikost2[0]):
                zmnozek = 0
                for z in range(velikost2[1]):
                    zmnozek += self[x, y] * matrika2[z, y]
                izhod.append(zmnozek)
        return BaseArray((velikost1[0], velikost2[1]), dtype=self.dtype, data=izhod)


# Za funkcijo merge_sort sem uporabil pomoč na tej povezavi https://www.geeksforgeeks.org/merge-sort/
def merge_sort(sklad = []):
    if len(sklad) > 1:
        mid = len(sklad) // 2
        sklad_levo = sklad[:mid]
        sklad_desno = sklad[mid:]
        merge_sort(sklad_levo)
        merge_sort(sklad_desno)
        i = j = k = 0

        while i < len(sklad_levo) and j < len(sklad_desno):
            if sklad_levo[i] < sklad_desno[j]:
                sklad[k] = sklad_levo[i]
                i += 1
            else:
                sklad[k] = sklad_desno[j]
                j += 1
            k += 1
        while i < len(sklad_levo):
            sklad[k] = sklad_levo[i]
            i += 1
            k += 1

        while j < len(sklad_desno):
            sklad[k] = sklad_desno[j]
            j += 1
            k += 1
    return sklad


def _multi_ind_iterator(multi_inds, shape):
    inds = multi_inds[0]
    s = shape[0]

    if type(inds) is slice:
        inds_itt = range(s)[inds]
    elif type(inds) is int:
        inds_itt = (inds,)

    for ind in inds_itt:
        if len(shape) > 1:
            for ind_tail in _multi_ind_iterator(multi_inds[1:], shape[1:]):
                yield (ind,)+ind_tail
        else:
            yield (ind,)


def _shape_to_steps(shape):
    dim_steps_rev = []
    s = 1
    for d in reversed(shape):
        dim_steps_rev.append(s)
        s *= d
    steps = tuple(reversed(dim_steps_rev))
    return steps


def _multi_to_lin_ind(inds, steps):
    i = 0
    for n, s in enumerate(steps):
        i += inds[n]*s
    return i


def _is_valid_indice(indice):
    if isinstance(indice, tuple):
        if len(indice) == 0:
            return False
        for i in indice:
            if not isinstance(i, int):
                return False
    elif not isinstance(indice, int):
        return False
    return True


def sestej(data1, data2, velikost1, velikost2):
    izhod = []
    if type(data2) == int and type(data1) == int:
        return data2+data1
    elif type(data2) == float and type(data1) == float:
        return data2+data1
    elif type(data2) == float and type(data1) == int:
        return data2+float(data1)
    elif type(data1) == float and type(data2) == int:
        return data1+float(data2)
    elif type(data1) == int or type(data1) == float:
        #sestevanje matrik s skalarjem
        for i in range(0,len(data2)):
            izhod.append(data2[i] + data1)
        return BaseArray(velikost2, data=izhod)
    elif type(data2) == int or type(data2) == float:
        #sestevanje matrik s skalarjem
        for i in range(0,len(data1)):
            izhod.append(data1[i] + data2)
        return BaseArray(velikost1, data=izhod)
    else:
        if(velikost1 == velikost2):
            for i in range(0,len(data1)):
                izhod.append(data1[i]+data2[i])
        return BaseArray(velikost1, data=izhod)
        #sestevanje matrik


def odstej(data1, data2, velikost1, velikost2):
    izhod = []
    if type(data2) == int and type(data1) == int:
        return data1 - data2
    elif type(data2) == float and type(data1) == float:
        return data1 - data2
    elif type(data2) == float and type(data1) == int:
        return data1 - float(data2)
    elif type(data1) == float and type(data2) == int:
        return data1 - float(data2)
    elif type(data1) == int or type(data1) == float:
        # sestevanje matrik s skalarjem
        for i in range(0, len(data2)):
            izhod.append(data1 - data2[i])
        return BaseArray(velikost2, data=izhod)
    elif type(data2) == int or type(data2) == float:
        # sestevanje matrik s skalarjem
        for i in range(0, len(data1)):
            izhod.append(data1[i] - data2)
        return BaseArray(velikost1, data=izhod)
    else:
        if (velikost1 == velikost2):
            for i in range(0, len(data1)):
                izhod.append(data1[i] - data2[i])
        return BaseArray(velikost1, data=izhod)
        # sestevanje matrik


def deli(data1, data2, velikost1, velikost2):
    izhod = []
    if type(data2) == int and type(data1) == int:
        if data2 == 0:
            raise Exception('Deljenje z 0 ni dovoljeno')
        return data1 / data2
    elif type(data2) == float and type(data1) == float:
        if data2 == 0:
            raise Exception('Deljenje z 0 ni dovoljeno')
        return data1 / data2
    elif type(data2) == float and type(data1) == int:
        if data2 == 0:
            raise Exception('Deljenje z 0 ni dovoljeno')
        return data1 / float(data2)
    elif type(data1) == float and type(data2) == int:
        if data2 == 0:
            raise Exception('Deljenje z 0 ni dovoljeno')
        return float(data1) / data2
    elif type(data1) == int or type(data1) == float:
        # sestevanje matrik s skalarjem
        for i in range(0, len(data2)):
            if data2[i] == 0:
                raise Exception('Deljenje z 0 ni dovoljeno')
            izhod.append(data1 / data2[i])
        return BaseArray(velikost2, dtype=float, data=izhod)
    elif type(data2) == int or type(data2) == float:
        # sestevanje matrik s skalarjem
        for i in range(0, len(data1)):
            if data2 == 0:
                raise Exception('Deljenje z 0 ni dovoljeno')
            izhod.append(data1[i] / data2)
        return BaseArray(velikost1, dtype=float, data=izhod)
    else:
        if (velikost1 == velikost2):
            for i in range(0, len(data1)):
                if data2[i] == 0:
                    raise Exception('Deljenje z {:d} ni dovoljeno'.format(data2[i]))
                izhod.append(data1[i] / data2[i])
        return BaseArray(velikost1, dtype=float, data=izhod)
        # sestevanje matrik


def logaritmiraj(data1, data2, velikost1, velikost2):
    izhod = []
    if type(data2) == int and type(data1) == int:
        if data1 <= 0 or data1 <= 0:
            raise Exception("Logaritmant ne sme biti manjsi ali enak 0")
        return math.log(data1, data2)
    elif type(data2) == float and type(data1) == float:
        if data1 <= 0 or data1 <= 0:
            raise Exception("Logaritmant ne sme biti manjsi ali enak 0")
        return math.log(data1, data2)
    elif type(data2) == float and type(data1) == int:
        if data1 <= 0 or data1 <= 0:
            raise Exception("Logaritmant ne sme biti manjsi ali enak 0")
        return math.log(float(data1), data2)
    elif type(data1) == float and type(data2) == int:
        if data1 <= 0 or data1 <= 0:
            raise Exception("Logaritmant ne sme biti manjsi ali enak 0")
        return math.log(data1, float(data2))
    elif type(data1) == int or type(data1) == float:
        if data1 <= 0:
            raise Exception("Logaritmant ne sme biti manjsi ali enak 0")
        # sestevanje matrik s skalarjem
        for i in range(0, len(data2)):
            if data2[i] <= 0:
                raise Exception("Osnova ne sme biti manjsa ali enaka 0")
            izhod.append(math.log(data1, data2[i]))
        return BaseArray(velikost2, dtype=float, data=izhod)
    elif type(data2) == int or type(data2) == float:
        if data2 <= 0:
            raise Exception("Osnova ne sme biti manjsa ali enaka 0")
        # sestevanje matrik s skalarjem
        for i in range(0, len(data1)):
            tmp = data1[i]
            if tmp <= 0:
                raise Exception("Logaritmant ne sme biti manjsi ali enak 0")
            izhod.append(math.log(tmp, data2))
        return BaseArray(velikost1, dtype=float, data=izhod)


def potenciraj(data1, data2, velikost1, velikost2):
    izhod = []
    if type(data2) == int and type(data1) == int:
        return data1**data2
    elif type(data2) == float and type(data1) == float:
        return data1**data2
    elif type(data2) == float and type(data1) == int:
        return data1**float(data2)
    elif type(data1) == float and type(data2) == int:
        return float(data1)**data2
    elif type(data1) == int or type(data1) == float:
        # sestevanje matrik s skalarjem
        for i in range(0, len(data2)):
            izhod.append(data1**data2[i])
        return BaseArray(velikost2, dtype=float, data=izhod)
    elif type(data2) == int or type(data2) == float:
        # sestevanje matrik s skalarjem
        for i in range(0, len(data1)):
            izhod.append(data1[i]**data2)
        return BaseArray(velikost1, dtype=float, data=izhod)

